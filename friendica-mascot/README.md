### Mascot for Friendica (https://friendi.ca)

![mascot preview](hare-single-alternative.png?raw=true)

![promo preview](Friendica-hares.png)

Files can usually be downloaded separately, no need to clone whole repo.
