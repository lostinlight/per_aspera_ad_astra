
## Open Source Job Fair

Landing for ULYSSIS

> Note: __/_static__ folder contains the generated output.

#### Setup
1. Run `npm install` in main project's folder

2. In terminal run `hexo server` in main folder, to start the server and preview at localhost
3. In second termnal, run `gulp` in  __*themes/starter*__ folder

  // This will start development theme mode, lint, serve and watch styles and scripts

4. Run `gulp build` to build styles and scripts for production

#### File Structure

- Develop styles in  __*themes/starter/assets/scss*__
- Develop scripts in __*themes/starter/assets/scripts*__

Many files in the setup, for example, `source/_posts` and `source/_data` folders, are needed only for correct work of Hexo (https://hexo.io/docs).
The setup is based on "Hexo starter": https://gitlab.com/lostinlight/ahoy-hexo.

#### Details
- All styles and markup are minified
- Images are minified
- Google Maps are replaced with Open Street Maps + Leaflet
- Email form uses https://getsimpleform.com, as an example of setting form submission on a static web page. Needs registration, in order to get all submissions to your email. Please, replace with your preferred action from your server.
