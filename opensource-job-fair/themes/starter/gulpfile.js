'use strict';

  let autoprefixer   = require('../../node_modules/gulp-autoprefixer'),
      babel          = require('../../node_modules/gulp-babel'),
      concat         = require('../../node_modules/gulp-concat'),
      cssnano        = require('../../node_modules/gulp-cssnano'),
      del            = require('../../node_modules/del'),
      eslint         = require('../../node_modules/gulp-eslint'),
      gulp           = require('../../node_modules/gulp'),
      gulpif         = require('../../node_modules/gulp-if'),
      imagemin       = require('../../node_modules/gulp-imagemin'),
      jpegRecompress = require('../../node_modules/imagemin-jpeg-recompress'),
      notify         = require('../../node_modules/gulp-notify'),
      plumber        = require('../../node_modules/gulp-plumber'),
      rename         = require('../../node_modules/gulp-rename'),
      runSequence    = require('../../node_modules/run-sequence'),
      sass           = require('../../node_modules/gulp-sass'),
      sourcemaps     = require('../../node_modules/gulp-sourcemaps'),
      stylelint      = require('../../node_modules/gulp-stylelint'), // https://github.com/olegskl/gulp-stylelint -- http://stylelint.io/user-guide/example-config/
      typeset        = require('../../node_modules/gulp-typeset'), // https://github.com/lucasconstantino/gulp-typeset
      uglify         = require('../../node_modules/gulp-uglify');


  function customPlumber (errTitle) {
    return plumber({
      errorHandler: notify.onError({
        title: errTitle || "Ouch! Error running Gulp",
        message: "Error: <%= error.message %>"
      })
    });
  }

   // Styles
   gulp.task('styles:lint', function() {
    return gulp.src('source/assets/scss/**/*.scss')
               .pipe(customPlumber('Ouch! Error Running StyleLint'))
               .pipe(stylelint({
                  failAfterError: false,
                  reporters: [
                    {formatter: 'verbose', console: true}
                  ]
                }));
  });

  gulp.task('styles:serve', function() {
    return gulp.src('source/assets/scss/main.scss')
               .pipe(sourcemaps.init())
               .pipe(customPlumber('Ouch! Error Running Sass'))
               .pipe(sass({errLogToConsole: true}))
               .pipe(autoprefixer(['last 2 versions', 'IE 9', 'iOS 8', 'Android 4']))
               .pipe(sourcemaps.write('./'))
               .pipe(gulp.dest('source/css'))
               .pipe(notify({ message: 'Hey, Captain, Styles task complete!', onLast: true }));
  });

  gulp.task('styles:build', function() {
    return gulp.src('source/assets/scss/main.scss')
               .pipe(customPlumber('Ouch! Error Running Sass'))
               .pipe(sass({errLogToConsole: true}))
               .pipe(autoprefixer(['last 2 versions', 'IE 9', 'iOS 8', 'Android 4']))
               .pipe(gulp.dest('source/css/'))
               .pipe(rename({ suffix: '.min' }))
               .pipe(cssnano())
               .pipe(gulp.dest('source/css/'))
  });

  // Scripts
  gulp.task('scripts:serve', function() {
    return gulp.src('source/assets/scripts/*.js')
               .pipe(sourcemaps.init())
               .pipe(babel({
                  presets: ['es2015']
                }))
               .pipe(customPlumber('Ouch! Error Running Scripts'))
               .pipe(eslint())
               .pipe(eslint.format())
               .pipe(concat('main.js'))
               .pipe(sourcemaps.write('./'))
               .pipe(gulp.dest('source/js/'))
               .pipe(notify({ message: 'Hey, Captain, Scripts task complete!', onLast: true }));
  });

  gulp.task('scripts:build', function() {
    return gulp.src('source/assets/scripts/*.js')
               .pipe(babel({
                  presets: ['es2015']
                }))
               .pipe(customPlumber('Ouch! Error Running Scripts'))
               .pipe(concat('main.js'))
               .pipe(rename({suffix: '.min'}) )
               .pipe(uglify())
               .pipe(gulp.dest('source/js/'))
  });

  // Vendor Scripts
  gulp.task('vendor', function() {
    return gulp.src('source/assets/scripts/vendor/*.js')
      .pipe(customPlumber('Ouch! Error Running Vendor'))
      .pipe(concat('vendor.js')) // only if http2 not configured on server
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(gulp.dest('source/js/'))
      .pipe(notify({ message: 'Vendor scripts bundled!', onLast: true }));
  });

  // Clean
  gulp.task('clean', function () {
    return del(['source/js/*', 'source/css/*']);
  });

  // Images
  gulp.task('images', function() {
    return gulp.src('../../source/img/**/*')
          .pipe(imagemin([
            imagemin.gifsicle(),
            jpegRecompress({
              loops: 4,
              min: 50,
              max: 95,
              quality:'high'
            }),
            imagemin.optipng(),
            imagemin.svgo()
          ]))
          .pipe(gulp.dest('../../source/img'))
  });

  // Watch
  gulp.task('watch', function() {
    gulp.watch('./source/assets/scss/**/*.scss', ['styles:serve']);
    gulp.watch('./source/assets/scripts/**/*.js', ['scripts:serve']);
  });

  // Development Mode
  gulp.task('default', function () {
      runSequence('styles:lint', 'styles:serve', 'scripts:serve', 'vendor', 'watch');
  });

  // Production Mode
  gulp.task('build', function (done) {
      runSequence('clean', 'styles:build', 'scripts:build', 'vendor', 'images', done);
  });
