### Logo exploration for Luakit (https://luakit.github.io)

![logo ideas preview](Luakit_ideas.jpg?raw=true)

This repo includes crude concepts for Luakit v.2 logo.
The discussion happens [here](https://github.com/luakit/luakit/issues/363).

Files can usually be downloaded separately, no need to clone whole repo.

This is how the icons would look on my Cinnamon desktop:

![logo ideas preview](Luakit_demo.jpg?raw=true)
