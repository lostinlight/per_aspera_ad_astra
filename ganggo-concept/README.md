
### Logo for GangGo (https://ggg.social)

![logo preview](icon-examples.jpg?raw=true)

> Animals ending with -go: flamingo

![logo preview](ganggo-v1-clr3.jpg?raw=true)

> Creatures containing -g(h)o: ghost.

Ghost icon is already in use by Snapchat, so it's important to differ.

![logo preview](ganggo-v2-clr.jpg?raw=true)

> Continuing the idea of G-letter form, an animal with a long tail (lemur):

![logo preview](ganggo-v3.jpg?raw=true)

And current logo variations, making outer form recognizable:

![logo preview](ganggo-v4-clr.jpg?raw=true)

![logo preview](ganggo-v4-clr-2.jpg?raw=true)

![logo preview](bw-1.jpg?raw=true)

Files can usually be downloaded separately, no need to clone whole repo.
