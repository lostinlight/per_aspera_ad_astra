
### Landing concept for friendi.ca

> More user-oriented, less developer-oriented

`Note:` this concept doesn't strive to be the best solution but aims at exploring ideas on how to change current version to make it friendlier for non-tech users new to decentralization concept

![Friendicaverse image](friendicaverse.png)

All images are under CC0 license, use them for any Friendica promo campaigns!

> loosely based on [joinmastodon.org](https://joinmastodon.org)

#### Current issues
__1.__ "friendica" (logo title and hero title) - everywhere else it's capitalized "Friendica"

__2.__ Font is a bit too thin

__3.__ Menu font size is small

__4.__ Font colour in the footer too light - hard to read

Please, see [screenshot](./landing-issues.jpg) for visual issues.

#### Main changes in concept variant:
__1.__ Logo colour is modified to use bright lemon-yellow, blue colour for highlights is also more vibrant; font used is Barlow Condensed Regular and Bold - [available](https://fonts.google.com/specimen/Barlow%20Condensed) for self-hosting;

__2.__ No "Home" link in main menu:  logo+title is a home link, always in the top left corner;

__3.__ "Apps" link: people are usually interested in mobile (and other) available apps, better place this link in main menu. "Apps" is shorter / simpler than "mobile clients" and allows to put other clients (desktop, etc) on same page;

__4.__ No "About" link in main menu: basic information is presented on landing page, more detailed info may be part of "Documentation" or "FAQ" category, hidden in menu dropdown;

__5.__ "Resources" menu dropdown is shortened: many current links may be part of one - "Documnation", this page may include "User guide" (First Steps, Use it, Community...) and "Developer Guide" (Requirements, Installation...);

__6.__ "Nodes" instead of "Find a server" - alternatively can be changed to "Servers". For consistency it's important to always refer to Friendica nodes either as "nodes" or as "servers" in all documentation and other pages - or to mention both terms (one in brackets). It's assumed that "Nodes" page will have a node picker similar to the one on Joinmastodon.org. Buttons "join a node" / "join Friendica" lead to this page. It's better to have such a picker than to direct users to Directory, as it's done now. The directory list may overwhelm - what is it, how to choose? A picker provides ways to choose quickly and easily - by category, language, node size / stability, etc;

__7.__ "Community" link is renamed "Team": current Community page is about "Developers' Community". As a user, I'd expect this page to be about online ways to interact with Friendica contributors - links to Community managers, chats, groups...;

__8.__ "Try it" button leads either to Node picker page or to a random (old, stable, small:) node;

__9.__ "How it works" button is an anchor to "How it works" part of landing;

__10.__ "Latest News" preview is truncated at certain ammount of letters, to make columns of same height;

__11.__ There's no word "privacy" anywhere in this landing concept. I think it's fair to say "Friendica is more private than <..something..>". But it's not true to say "Friendica is private". Newcomers may not realize that all limited posts will be available to their own administrator and all admins the post was federated to. Newcomers may also not know that in Friendica one-to-one "mail" is non-retractable - some people may consider this a
privacy violation. And of course, there's no encryption. For these reasons instead of "private" I'd try to use more precise wording - "limited view", "seen only by...", "non-public".
