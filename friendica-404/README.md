### Friendica 404 page exploration

Fun dark theme exploration
![screenshot preview](404.jpg)

Idea with bubbles used as promo material

![screenshot preview](friendica-promo-bubbles-small.jpg)

You will find minified svg files in /svg folder.

Vier theme example
![screenshot preview](friendica-vier-404.jpg)

Files can usually be downloaded separately, no need to clone whole repo.
