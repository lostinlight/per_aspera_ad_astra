### Much design, such wow

This is a general repo for design-ish things, concepts, ideas, projects.

Files can usually be downloaded separately, no need to clone whole repo.

If you ever feel the need to ask something, please, do so in issues.

