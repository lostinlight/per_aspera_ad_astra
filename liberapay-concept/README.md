### Design exploration for Liberapay (liberapay.com)

![liberapay landing preview](lp-clrs-example.jpg?raw=true)

Here's a .psd file mentioned in [this discussion](https://github.com/liberapay/liberapay.com/issues/608), just in case anyone needs it.

The .psd lacks some icons in proper format, if needed, please, let me know, and I'll add the assets to the repo.

Files can usually be downloaded separately, no need to clone whole repo.
