
### [Socialhome](https://socialhome.network) redesign

`Note:` this concept doesn't strive to be the best solution but aims at exploring ideas on how to change interface to achieve best user experience

#### Icons and fonts
* This design uses [IonIcons](https://github.com/ionic-team/ionicons) that I personally find more interesting than FontAwesome icons.
* These screens use *Lato family* - Regular and Bold, both very well-readable.

#### Key changes
__1.__ Background changed to light. Light posts on dark background don't look consistent. A separate fully dark theme may be created. Please, bare in mind that many common users (who don't spend hours in the console:) find it tiring to read long form text in dark UI, it's always safe to provide light theme first. Colours used are mostly light / dark plus two logo colours as highlights.

__2.__ Network name in special font. This adds character to the official design (see [Pixabay](https://pixabay.com) example). No need to add extra font - a single small svg will do the trick.

__3. Menu changes__:

  __a)__ Search bar given a more prominent centre position

  __b)__ "Create" left as main call-to-action and most important link that should be always at hand. "Notifications" and "Messages" added to main menu - two options reflecting user's interactions and thus important to user. "My profile" and "Contacts" hidden in Settings dropdown

__4. Settings dropdown__:

  __a)__ "Streams" renamed to "News". This is a personal preference. Common users know what "feed" and "timeline" mean. No need to add extra (diaspora) terminology. "News" is a short word everyone will understand

  __b)__ "Account", "API token", "Emails" are incorporated into one link - "Settings". All these are user settings, more will appear with time. Listing them all in the dropdown may quickly become a mess. One "settings" page for all kinds of settings will help user easily find where to change things. Settings page may include "account", "security", "privacy" and other categories

__5. Profile header__:

  __a)__ Made full width, to accomodate "Background" feature. It's a nice-to-have feature for a network that aims to be one's personal web page. This will also eliminate bad-looking situations [like this one](./current/ref-1.png)

  __b)__ Includes input field for a short description (limited characters) and a limited ammount of external links (3 in this case)

__6. Profile options__:

  __a)__ *My profile* has "Edit" option. Same edit functionality may be provided on "Profile" /settings page. But we also need a button right next to the profile for a quick action

  __b)__ *Other profile* has options "Follow", "Bookmark", "Message", "Hide", "Block"

  __c)__ "Bookmark" feature allows to save user's profile for later reference without following them. Same feature is provided for single posts (star icon). "Bookmarks" link should be added to main menu dropdown (below "News")

  __d)__ "Hide" feature may be a popup explaining the action "This will hide user's posts from your News. You may still follow them -> OK / Cancel"

  __e)__ "Block" feature may be a popup explaining the action "This will hide all user's posts from your News. It will also prevent blocked user from interacting with you or your posts -> OK / Cancel"

__7. Grid__:

  __a)__ Three columns, same width, different height

  __b)__ Grid layout is best for image content. However, long text posts are harder to perceive in a grid (personal experience). Thus, post height should be somehow restricted in preview mode. For example, hide long text post under "read more". If post has at least 1 attached image, programmaticaly check if it's landscape or portrait mode. If landscape, add (first) image to top and allow several lines of text preview - the rest goes under "read more". If the image is portrait or square,  add it to top and hide the rest of the post under "read more"

  __c)__ Some people prefer "one column" layout, so there may be a feature to switch between single- and multi-column. It may be an option in settings and also an icon for a quick change (see icon in "News" one-column example screen)

__8. Post options__

  __a)__ Four types of interaction: "like", "reshare", "comment" and "bookmark". I decided against "emoji reactions in posts" as a useless option. The UI, though bulky with many emoji options, is possible. However, an 'angry smile' reaction to your post will give you zero information about what or why the user meant, leaving no space for constructive argument. Emojis option is added as part of comments (and should be available in post WYSIWYG for the post's author as well)

  __b)__ *My profile* has common "edit" and "delete" options. Dropdown menu allows adding more options if need be in the future

  __c)__ *Other profile* has "Source" and *Report* options. Dropdown menu allows adding more options if need be in the future. "Source" is a link to post source which is handy to have. Post timestamp leads to post preview locally (so no link to source easily available)

#### "Create" page suggestions

__1. Preview__: provide post preview as option on icon click - in the example it's  positioned top right, which will probably be convenient on mobile devices. It focuses user's attention on writing the post and frees much space on desktop.

__2. Title__: add separate (optional) title input. The majority of regular users do not know markdown and won't use titles. Socialhome's grid layout would be easier to browse with a human's eye if more posts had titles - like a newspaper. This is especially true for long-form posts. Adding this input will most likely motivate more users to add titles to their posts.

__3. Options__: leave main post options, hide the rest in "More" dropdown. Get rid of long option explanations:  explain "yes-no" option's meaning in a tooltip on hover - eg., explain "Federate" checkmark option in this way.

If an important multi-option needs explanation - like "visibility" - make it a stand-alone dropdown and hide explanations inside. Choosing any visibility option closes the dropdown and sets the visibility to chosen one (with its specific icon marked with bright colour to highlight the choice).

__4. Visibility__: In this example I rephrased some visibility explanations, making them shorter and simpler. Also renamed a couple of them (personal preference).

__5. Icons__: Unexpectedly, there're no "bold", "italic" icons in IonIcons collection. If Socialhome plans to use svgs instead of icon font and to bundle its svg collection with tools (minifying size, using only necessary icons), IonIcons can still be used, adding a bunch of missing needed svgs from elsewhere. If the project plans to continue using icon font, probably FontAwesome is the best choice.
