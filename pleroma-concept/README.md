## Logo concept for Pleroma (https://pleroma.social)

![logo preview](Pleroma-example.jpg?raw=true)

> Inspired by the Forest God from "princess Mononoke", resembling Kirin, also dragon may be

#### Why
- animal as logo / mascot allows to create various beautiful branding materials later (think Mastodon and its PressKit images)
- "Pleroma", among other things, means "to complete an incomplete thing" (wikipedia). This takes much power. Kirin is a powerful creature, also a good one.

This is how a simplified icon would look like, for example, in a social button:

![button preview](pleroma-icon-example.jpg?raw=true)

Here are some UI examples, for visualization: [one](PleromaPeach.jpg), [two](PleromaOtherColours.jpg).
In the UI with slightly modified colours, the font for "Pleroma" is Woodford Bourne Regular.

There are also .png and .svg versions in the repo.

This is a concept. Please, let me know what you think.

![logo preview](Pleroma-variants.jpg?raw=true)

Files can usually be downloaded separately, no need to clone whole repo.
