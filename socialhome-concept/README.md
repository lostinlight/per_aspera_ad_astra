### Logo for Social Home (https://socialhome.network)

![logo preview](Sh-text-context.jpg?raw=true)

This repo shows various steps of Socialhome logo design.
The discussion happens [here](https://joindiaspora.com/posts/10353416).

Files can usually be downloaded separately, no need to clone whole repo.
